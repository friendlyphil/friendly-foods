import Vue from 'vue'
import App from './App.vue'
import FoodGroup from './FoodGroup.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
